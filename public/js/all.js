function calculate(e) {
    if (e.keyCode == 13) {
        _calculate();
    }
}

function validate(nominal) {
    if(nominal.toLowerCase().slice(-2) == "rp") {
        return [false, "Wrong format"];
    } else {
        var temp = nominal.replace(/[rp.]/gi,'').trim();
        if(temp.length == 0) {
            return [false, "Empty nominal"];
        } else {
            if(isNaN(temp)) {
                return [false, "Wrong format"];
            } else {
                if(temp < 0) {
                    return [false, "Nominal can not be minus"];
                } else {
                    return [true, (parseInt(temp))];
                }
            }
        }
    }
}

function count(money, value){
    var jumlah = 0;
    var selisih = 0;

    if(value % money === 0){
        jumlah = value/money;
        selisih = 0;
    } else {
        jumlah = Math.floor(value/money);
        selisih = value - (money*jumlah);
    }

    return [jumlah, selisih];
}


var _calculate = function() {

    $('#result').empty();
    $('#error').empty();

    var money = [50, 100, 500, 1000, 5000, 10000, 20000, 50000, 100000];

    var checkValidate = validate($('#nominal').val());

    if(checkValidate[0] === false) {
        $("#error").append(checkValidate[1]);
    } else {

        var value = checkValidate[1];

        for(var i = money.length-1; i >= 0; i--) {
            if(money[i] <= value) {
                var temp = count(money[i], value);
                var jumlah = temp[0];
                value = temp[1];
                $("#result").append(jumlah + "x Rp " + money[i].toLocaleString('id') + "<br>");
                if(value != 0 && value <= money[0]) {
                    $("#result").append("Left Rp " + value + " (no available fraction)<br>");
                }
            }
        }
    }
}