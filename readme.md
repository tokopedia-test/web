Tokopedia Software Engineer Mobile Web Test
-------------

## PHP version

7.0.0

## System depedencies

Pastikan sudah ada composer, git, nodejs dan npm.

Setelah cloning aplikasi menggunakan perintah `git clone`, change directory ke folder project. Jadi present working directory berada di root folder project.

Lalu jalankan perintah `composer install` untuk meminta composer mendownload semua packages dependencies. Packages ini adalah libraries untuk server-side application.

Lalu, jalankan `npm install`. Ini untuk proses build assets for production.

## File preparation

Copy file `.env.production` ke file `.env`. Bisa dengan perintah `cp .env.production .env`. Kedua file ini ada di root folder project.

Jadi hasil akhirnya adalah `/.env`.

## Copy dependecies

Jalankan `npm run dev` untuk melakukan generate patternfly JS, CSS, dan copy CSS dan image dari patternfly.

## Directory permissions

Jalankan `chmod -R 777 storage bootstrap/cache`.

## Run project

Untuk menjalankan aplikasi, gunakan

```php artisan serve```

pada directory root project.

###### Copyright &copy; 2019. Jonathan.