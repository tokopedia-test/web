<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <style>
            @font-face {
                font-family: Rubik;
                src: url('/fonts/Rubik-Regular.ttf');
                font-weight: 400;
            }
            @font-face {
                font-family: Rubik;
                src: url('/fonts/Rubik-Medium.ttf');
                font-weight: 500;
            }

            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Rubik';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
                font-size: 30px;
            }

            .tagline {
                text-align: center;
                display: inline-block;
                font-size: 18px;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
                font-weight: 500;
                margin-top: 30px;
            }

            .error-page {
                background: url('images/503.jpg');
                background-size: cover;
            }
        </style>
    </head>
    <body>
        <div class="container error-page">
            <div class="content">
                <img src="{{ asset('images/xblank.jpg') }}" class="img-responsive center-block">
                <div class="title">Sorry!</div>
                <div class="content">We'll be right back.</div>
                <br>
                <div class="tagline">We are performing some maintenance. We should be back online shortly.</div>
            </div>
        </div>
    </body>
</html>
