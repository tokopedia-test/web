@extends('layouts.backend')

@section('content')
    <div class="content">
        <div class="row">
            <div class="col-lg-6">
                <span class="font-size-2x rubik-medium">Input your nominal:</span>
                <input type="text" class="form-control" id="nominal" name="nominal" placeholder="Enter nominal here..." autocomplete="off" onkeyup="calculate(event)" >
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-lg-6">
                <span class="font-size-2x rubik-medium">Results:</span>
                <div id="result"></div>
                <div id="error" class="text-danger font-italic"></div>
            </div>
        </div>
    </div>
@endsection