<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- jQuery -->
    <script src="{{ asset('js/backend.js') }}"></script>
    <script src="{{ asset('js/all.js') }}"></script>

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/backend.css') }}">

    @yield('css')
  </head>
  <body>
    <div>
        <div class="d-flex justify-content-between align-items-center top-nav">
            <img src="{{ asset('images/tokopedia.png') }}"  width="150">
            Tokopedia Software Engineer Mobile Web Test
        </div>
        <div class="container-fluid">
            @yield('content')
        </div>
        <div class="footer font-smaller text-grey">
            Created by <a href="https://linkedin.com/in/jonathanjsh/">Jonathan</a>
            for <span class="text-branding">Tokopedia Software Engineer Mobile Web Test</span>
            &copy; 2019.
        </div>
    </div>
    <!-- ./wrapper -->
    <script>
    $(function () {

    });
    </script>
  </body>
</html>